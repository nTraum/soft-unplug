# Soft unplug

Sets volume to low when unplugging headphones.
Fixes headphone noise by adjusting mic boost to 22%.

## Installation

Copy or link events files to /etc/acpi/events
Copy or link actions files to /etc/acpi

Restart acpi daemon: `sudo systemctl restart acpid.service`
